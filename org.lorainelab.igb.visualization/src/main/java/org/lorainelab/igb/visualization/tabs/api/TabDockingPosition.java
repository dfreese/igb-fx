package org.lorainelab.igb.visualization.tabs.api;

/**
 *
 * @author dcnorris
 */
public enum TabDockingPosition {
    BOTTOM,
    RIGHT
}
